#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

void DrawAsterrisk(int);

int main(){
  unsigned int Figures[10] = {18, 20, 32, 29, 4, 0, 29, 12, 9, 10};

  cout << "Index" << setw(8) << "Value" << setw(14) << "Histogram" << endl << endl;

  for (int i = 0; i < 10; i++){
    cout << setw(5) << i+1 << setw(8)  << Figures[i] << setw(6);
    DrawAsterrisk(Figures[i]);
    cout << endl;
  }

  // another setw
  int BaseNo, ExpoNo;
  cin >> BaseNo >> ExpoNo;
  cout << setw(10) << setiosflags(ios::fixed) << pow(BaseNo, ExpoNo) << endl;
  
  return 0;
}


void DrawAsterrisk(int x){
  for (int j = 0; j <= x; j++)
    cout << "*" ;
}
