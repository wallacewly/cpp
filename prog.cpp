// reference must be initialized

#include <iostream>
using namespace std;

int &Func(void)
{
  static int a = 0;
  cout << "a == " << a <<endl;
  return a;
}

int main(){
  int x = 3, &y=x, z;
  cout << "x = " << x << endl << "y = " << y << endl;
  y = 7;
  cout << "x = " << x << endl << "y = "<< y <<endl;

  z = y;
  cout << "z = " << z << endl;

  cout << "\n A function returning a reference: \n\n";

  Func();
  Func() = 5;
  Func() += 5;
  Func();

  cout << "\n\nPress ENTER to exit.\n";
  cin.get();
}
